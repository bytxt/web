<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2018 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
namespace app\portal\controller;

use cmf\controller\HomeBaseController;
use app\portal\service\PostService;
use app\portal\model\PortalCategoryModel;

class IndexController extends HomeBaseController
{
    public function index()
    {
		$postService         = new PostService();

        $article    = $postService->publishedArticle(5);
		 $portalCategoryModel = new PortalCategoryModel();

        $category = $portalCategoryModel->where('id', 6)->where('status', 1)->find();
       
        $this->assign('category', $category);
		$this->assign('article', $article);
        return $this->fetch(':index');
    }
}
