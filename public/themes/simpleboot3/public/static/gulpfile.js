var gulp = require('gulp'),
    sass = require('gulp-sass'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require("browser-sync").create(),
    reload = browserSync.reload;


gulp.task('styles', function () {
    return gulp.src(['css/main.scss'])
        .pipe(sass())
        .pipe(minifycss())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function () {
    gulp.watch('css/main.scss', ['styles']);
});
gulp.task('serve', ['styles'], function () {
    browserSync.init({
        server: "./"
    });

    gulp.watch(['*.html']).on('change', browserSync.reload);
    gulp.watch('css/main.scss', ['styles']);
});

gulp.task('default', ['serve']);